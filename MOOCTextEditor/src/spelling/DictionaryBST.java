package spelling;

import java.util.TreeSet;

/**
 * @author UC San Diego Intermediate MOOC team
 *
 */
public class DictionaryBST implements Dictionary 
{
   private TreeSet<String> dict;
	
    // TODO: Implement the dictionary interface using a TreeSet.  
 	// You'll need a constructor here
   public DictionaryBST(){
	   dict = new TreeSet<String>();
   }
	
    
    /** Add this word to the dictionary.  Convert it to lowercase first
     * for the assignment requirements.
     * @param word The word to add
     * @return true if the word was added to the dictionary 
     * (it wasn't already there). */
    public boolean addWord(String word) {
    	// TODO: Implement this method
    	//change case
    	String wordToAdd = word.toLowerCase();
    	//if already in the dictionary
    	if(dict.contains(wordToAdd)){
    		//do nothing and return false
    		return false;
    	}//if not, add and return true
    	else{
    		dict.add(wordToAdd);
    	}
        return true;
    }


    /** Return the number of words in the dictionary */
    public int size()
    {
    	// TODO: Implement this method
        return dict.size();
    }

    /** Is this a word according to this dictionary? */
    public boolean isWord(String s) {
    	//TODO: Implement this method
    	//change case
    	String wordToCheck = s.toLowerCase();
    	//return true if it contains or false if otherwise
        return dict.contains(wordToCheck);
    }

}
