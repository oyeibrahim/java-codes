package textgen;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

/** 
 * An implementation of the MTG interface that uses a list of lists.
 * @author UC San Diego Intermediate Programming MOOC team 
 */
public class MarkovTextGeneratorLoL implements MarkovTextGenerator {

	// The list of words with their next words
	private List<ListNode> wordList; 
	
	// The starting "word"
	private String starter;
	
	// The random number generator
	private Random rnGenerator;
	
	public MarkovTextGeneratorLoL(Random generator)
	{
		wordList = new LinkedList<ListNode>();
		starter = "";
		rnGenerator = generator;
	}
	
	//to test without random
	public MarkovTextGeneratorLoL()
	{
		wordList = new LinkedList<ListNode>();
		starter = "";
	}
	
	
	/** Train the generator by adding the sourceText */
	@Override
	public void train(String sourceText)
	{
		// TODO: Implement this method
		//split into words
		String[] sourceWords = sourceText.split("\\s+");
		//instantiate a new node to be used in the method
		ListNode node;
		//set starter to the first word
		starter = sourceWords[0];
		String prevWord = starter;
		//start at the second word since starter already has the first word
		for(int i = 1; i < sourceWords.length; i++){
			//get word at this position
			String temp = sourceWords[i];
			//check if the word is already a node
			//using helper method
			if(containsNode(prevWord)){
				//if it is, add another next word
				//using helper method
				int index = containsWord(prevWord);
				wordList.get(index).addNextWord(temp);
			} else {
				//if not, create a new node
				node = new ListNode(prevWord);
				//add it to wordlist
				wordList.add(node);
				//add its next word
				node.addNextWord(temp);
			}
			//increment to make the loop end
			prevWord = temp;
			
		}
		
		/*code to make the next word for the last word be the first word*/
		//get the last word
		String lastWord = sourceWords[sourceWords.length - 1];
		//if it has a node before
		if(wordList.contains(lastWord)){
			//add new nextWord
			int index = containsWord(lastWord);
			wordList.get(index).addNextWord(starter);
		} else {
			//if not, create a node and add next word
			node = new ListNode(lastWord);
			wordList.add(node);
			node.addNextWord(starter);
		}
	}
	
	/** 
	 * Generate the number of words requested.
	 */
	@Override
	public String generateText(int numWords) {
	    // TODO: Implement this method
		//set current word to the starting word which has been set by the train() method
		String currWord = starter;
		//to hold output
		String output = "";
		//add current word to output and add a space in front
		output += currWord + " ";
		//track number of words added
		int wordsCount = 1;
		//if no word in WordList then we have nothing to select from
		if (wordList.isEmpty()){
			return "No word jare";
		}
		//ensure we get the number of words needed as passed in to the method through the @param
		while (wordsCount < numWords){
			//if the current word has a node in wordList
			if(containsNode(currWord)){
				//get the index
				int index = containsWord(currWord);
				//use the index to get its next words and use random to select a random word from its next words
				String randomWord = wordList.get(index).getRandomNextWord(rnGenerator);
				//add the word selected to output and add a space
				output += randomWord + " ";
				//this is to ensure we get the next word for the current last word
				currWord = randomWord;
				//increment number of words
				wordsCount++;
			}
		}
		
		return output;
	}
	
	
	// Can be helpful for debugging
	@Override
	public String toString()
	{
		String toReturn = "";
		for (ListNode n : wordList)
		{
			toReturn += n.toString();
		}
		return toReturn;
	}
	
	/** Retrain the generator from scratch on the source text */
	@Override
	public void retrain(String sourceText)
	{
		// TODO: Implement this method.
		//create new list. this will erase any previous content of the list
		wordList = new LinkedList<>();
		//reset starter to null
		starter = "";
		//train on the passed in source text
		train(sourceText);
	}
	
	// TODO: Add any private helper methods you need here.
	
	/** Gets the index of a word if contained in wordList */
	public int containsWord(String word)
	{
		for(int index = 0; index < wordList.size(); index++) {
			if(wordList.get(index).getWord().equals(word))
				return index;
		}
		return -1;
	}
	
	/** Checks if a node is contained in wordList */
	public boolean containsNode(String word)
	{
		for(int index = 0; index < wordList.size(); index++) {
			if(wordList.get(index).getWord().equals(word))
				return true;
		}
		return false;
	}
	
	
	/**
	 * This is a minimal set of tests.  Note that it can be difficult
	 * to test methods/classes with randomized behavior.   
	 * @param args
	 */
	public static void main(String[] args)
	{
		// feed the generator a fixed random value for repeatable behavior
		/*MarkovTextGeneratorLoL gen = new MarkovTextGeneratorLoL(new Random(42));
		String textString = "Hello.  Hello there.  This is a test.  Hello there.  Hello Bob.  Test again.";
		System.out.println(textString);
		gen.train(textString);
		System.out.println(gen);
		System.out.println(gen.generateText(20));
		String textString2 = "You say yes, I say no, "+
				"You say stop, and I say go, go, go, "+
				"Oh no. You say goodbye and I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello. "+
				"I say high, you say low, "+
				"You say why, and I say I don't know. "+
				"Oh no. "+
				"You say goodbye and I say hello, hello, hello. "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello. "+
				"Why, why, why, why, why, why, "+
				"Do you say goodbye. "+
				"Oh no. "+
				"You say goodbye and I say hello, hello, hello. "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello. "+
				"You say yes, I say no, "+
				"You say stop and I say go, go, go. "+
				"Oh, oh no. "+
				"You say goodbye and I say hello, hello, hello. "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello, hello, hello,";
		System.out.println(textString2);
		gen.retrain(textString2);
		System.out.println(gen);
		System.out.println(gen.generateText(20));
		System.out.println();*/
		System.out.println("My test below");
		System.out.println();
		String word = "hi there hi Leo";
		
		MarkovTextGeneratorLoL gen2 = new MarkovTextGeneratorLoL(new Random(3));
		gen2.train(word);
		System.out.println(gen2);
		System.out.println(gen2.generateText(4));
		
	}

}

/** Links a word to the next words in the list 
 * You should use this class in your implementation. */
class ListNode
{
    // The word that is linking to the next words
	private String word;
	
	// The next words that could follow it
	private List<String> nextWords;
	
	ListNode(String word)
	{
		this.word = word;
		nextWords = new LinkedList<String>();
	}
	
	public String getWord()
	{
		return word;
	}

	public void addNextWord(String nextWord)
	{
		nextWords.add(nextWord);
	}
	
	public String getRandomNextWord(Random generator)
	{
		// TODO: Implement this method
	    // The random number generator should be passed from 
	    // the MarkovTextGeneratorLoL class
		return nextWords.get(generator.nextInt(nextWords.size()));
	}

	public String toString()
	{
		String toReturn = word + ": ";
		for (String s : nextWords) {
			toReturn += s + "->";
		}
		toReturn += "\n";
		return toReturn;
	}
	
}


