package textgen;

import java.util.AbstractList;


/** A class that implements a doubly linked list
 * 
 * @author UC San Diego Intermediate Programming MOOC team
 *
 * @param <E> The type of the elements stored in the list
 */
public class MyLinkedList<E> extends AbstractList<E> {
	LLNode<E> head;
	LLNode<E> tail;
	int size;

	/** Create a new empty LinkedList */
	public MyLinkedList() {
		// TODO: Implement this method
		size = 0;
		head = new LLNode<E>(null);
		tail = new LLNode<E>(null);
		head.next = tail;
		tail.prev = head;
	}

	/**
	 * Appends an element to the end of the list
	 * @param element The element to add
	 */
	public boolean add(E element ) 
	{
		// TODO: Implement this method
		if(element == null){
			//use the same exception you will test against in JUnit
			//throw new IndexOutOfBoundsException("MyLinkedList object cannot store null pointers");
			throw new NullPointerException("MyLinkedList object cannot store null pointers");
		}
		//steps to add new element and connect them
		LLNode<E> lastNode = tail.prev;
		LLNode<E> node = new LLNode<E>(element, lastNode, tail);
		lastNode.next = node;
		tail.prev = node;
		
		size++;
		
		return true;
	}

	/** Get the element at position index 
	 * @throws IndexOutOfBoundsException if the index is out of bounds. */
	public E get(int index) 
	{
		// TODO: Implement this method.
		return getNode(index).data;
	}

	/**
	 * Add an element to the list at the specified index
	 * @param The index where the element should be added
	 * @param element The element to add
	 */
	public void add(int index, E element ) 
	{
		// TODO: Implement this method
		//chck for error
		if (element == null || index < 0){
			throw new NullPointerException("MyLinkedList object cannot store null pointers");
		}
		//if there is no element in the list before and the new element is to be
		//added as the first element, just call the add() method above
		//which adds without requesting index.
		if (index == 0 && size == 0){
			add(element);
			return;
		}
		
		//steps to add new element and connect them
		LLNode<E> currentNode = getNode (index);
		LLNode<E> previousNode = currentNode.prev;
		
		LLNode<E> node = new LLNode<E> (element, previousNode, currentNode);
		previousNode.next = node;
		currentNode.prev = node;
		
		size++;
	}


	/** Return the size of the list */
	public int size() 
	{
		// TODO: Implement this method
		return size;
	}

	/** Remove a node at the specified index and return its data element.
	 * @param index The index of the element to remove
	 * @return The data element removed
	 * @throws IndexOutOfBoundsException If index is outside the bounds of the list
	 * 
	 */
	public E remove(int index) 
	{
		// TODO: Implement this method
		if (index < 0){
			throw new NullPointerException("MyLinkedList object cannot access null pointers");
		}
		LLNode<E> currentNode = getNode(index);
		LLNode<E> previousNode = currentNode.prev;
		LLNode<E> nextNode = currentNode.next;

		previousNode.next = nextNode;
		nextNode.prev = previousNode;

		size--;
		return currentNode.data;
	}

	/**
	 * Set an index position in the list to a new element
	 * @param index The index of the element to change
	 * @param element The new element
	 * @return The element that was replaced
	 * @throws IndexOutOfBoundsException if the index is out of bounds.
	 */
	public E set(int index, E element) 
	{
		// TODO: Implement this method
		if(element == null)
			throw new NullPointerException();

		LLNode<E> node = getNode(index);
		E returnData = node.data;

		node.data = element;
		return returnData;
	}  
	/**
	 * Helper method for method get() to get elements at an index
	 * @param index The index of the element needed
	 * @return The element at that index
	 * @throws IndexOutOfBoundsException if the index is out of bounds.
	 */
	private LLNode<E> getNode(int index){
		//check for errors
		//we are using size-1 here even though size doesn't begin at zero. this is
		//because position variable used in the loop below start at zero to follow
		//the normal programming pattern of starting everything at zero.
		//it is the position variable that we use to get the index.
		if(index > size-1 || index < 0){
			//use the same exception you will test against in JUnit
			//throw new IndexOutOfBoundsException("MyLinkedList object cannot store null pointers");
			throw new NullPointerException("MyLinkedList object cannot store null pointers");
		}
		
		//create temporary holders
		LLNode<E> prevNode = head;
		LLNode<E> currentNode;
		
		//loop through nodes to get the index wanted
		for (int position = 0; position < size; position++){
			//make currentNode be the first node (item) in the LinkedList at first
			//then it will continue changing to the next items through iteration
			currentNode = prevNode.next;
			//when we get to the wanted index
			if(position == index) {
				//return the node as the answer
				return currentNode;
			}
			//this is to increment the iteration and make currentNode become the
			//next item on the list
			prevNode = currentNode;
		}
		//if we can't find the index (which may be impossible cause we checked for 
		//error at the beginning), return any other thing
		return null;
	} 
	
	public static void main(String[] args) {
		MyLinkedList<Integer> emptyList = new MyLinkedList<Integer>();
		emptyList.add(20);
		emptyList.add(21);
		emptyList.add(22);
		emptyList.add(23);
		emptyList.add(24);
		int ans = emptyList.get(4);
		System.out.println(ans);
		int size = emptyList.size();
		System.out.println("size = " + size);
	}
}

class LLNode<E> 
{
	LLNode<E> prev;
	LLNode<E> next;
	E data;

	// TODO: Add any other methods you think are useful here
	// E.g. you might want to add another constructor
	public LLNode(E e, LLNode<E> prev, LLNode<E> next) 
	{
		this.data = e;
		this.prev = prev;
		this.next = next;
	}

	public LLNode(E e) 
	{
		this.data = e;
		this.prev = null;
		this.next = null;
	}

}


